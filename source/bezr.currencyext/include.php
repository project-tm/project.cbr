<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;

define('CURRENCY_EXT_MODULE_NAME', 'bezr.currencyext');

if (!Loader::includeModule('currency')) {
    return false;
}

Loader::registerAutoLoadClasses(CURRENCY_EXT_MODULE_NAME, array(
    // no thanks, bitrix, we better will use psr-4 than your class names convention
    'Bezr\CurrencyExt\Rates\Update' => 'lib/rates/Update.php',
));
