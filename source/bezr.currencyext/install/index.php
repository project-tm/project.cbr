<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

if (class_exists('bezr_currencyext')) {
    return;
}

class bezr_currencyext extends CModule
{
    /** @var string */
    public $MODULE_ID;

    /** @var string */
    public $MODULE_VERSION;

    /** @var string */
    public $MODULE_VERSION_DATE;

    /** @var string */
    public $MODULE_NAME;

    /** @var string */
    public $MODULE_DESCRIPTION;

    /** @var string */
    public $MODULE_GROUP_RIGHTS;

    /** @var string */
    public $PARTNER_NAME;

    /** @var string */
    public $PARTNER_URI;

    public function __construct()
    {
        $this->MODULE_ID = 'bezr.currencyext';
        $this->MODULE_VERSION = '0.1.0';
        $this->MODULE_VERSION_DATE = '2015-04-24 16:23:14';
        $this->MODULE_NAME = Loc::getMessage('BCE_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('BCE_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = "Павел Безруков";
        $this->PARTNER_URI = "http://bezr.pro";
    }

    public function doInstall()
    {
        if (!Loader::includeModule('currency')) {
            CAdminMessage::showMessage(Loc::getMessage("BCE_NOT_INSTALLED_DEPENDENCE"));
            return false;
        }

        $rsCurrency = CCurrency::GetList($by = 'sort', $order = 'asc');
        $arCurrencies = Array();
        while ($arCurrency = $rsCurrency->GetNext(false, false)) {
            if ($arCurrency['BASE'] != 'Y') {
                $arCurrencies[] = $arCurrency['CURRENCY'];
            }
        }

        Option::set($this->MODULE_ID, 'rates_update_interval',
            Option::get($this->MODULE_ID, 'rates_update_interval'));

        $ratesUpdateCurrency = Option::get($this->MODULE_ID, 'rates_update_currency');
        Option::set($this->MODULE_ID, 'rates_update_currency',
            serialize($ratesUpdateCurrency ?: $arCurrencies));

        ModuleManager::registerModule($this->MODULE_ID);
        $this->installAgents();
    }

    public function installAgents()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            CAgent::AddAgent('\Bezr\CurrencyExt\Rates\Update::run();',
                $this->MODULE_ID,
                'Y',
                Option::get($this->MODULE_ID, 'rates_update_interval')
            );
        }
    }

    public function doUninstall()
    {
        $this->uninstallAgents();
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

    public function uninstallAgents()
    {
        CAgent::RemoveModuleAgents($this->MODULE_ID);
    }
}
