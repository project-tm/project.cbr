<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'bezr.currencyext');

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Text\String;

if (!$USER->isAdmin()) {
    $APPLICATION->authForm('Nope');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

if (!Loader::includeModule('currency')) {
    return false;
}

Loc::loadMessages($context->getServer()->getDocumentRoot() . "/bitrix/modules/main/admin/settings.php");
Loc::loadMessages($context->getServer()->getDocumentRoot() . "/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$tabControl = new CAdminTabControl("tabControl", array(
    array(
        "DIV" => "edit1",
        "TAB" => Loc::getMessage("MAIN_TAB_SET"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_SET"),
    ),
));

$arRatesUpdateAgent = CAgent::GetList(Array("ID" => "DESC"), array("NAME" => '\Bezr\CurrencyExt\Rates\Update::run();'))->GetNext(false, false);

if ((!empty($save) || !empty($restore)) && $request->isPost() && check_bitrix_sessid()) {
    if (!empty($restore)) {
        Option::delete(ADMIN_MODULE_NAME);
        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_RESTORED"),
            "TYPE" => "OK",
        ));
    } elseif ($request->getPost('rates_update_interval') && $request->getPost('rates_update_currency')) {

        Option::set(
            ADMIN_MODULE_NAME,
            "rates_update_interval",
            $request->getPost('rates_update_interval')
        );
        Option::set(
            ADMIN_MODULE_NAME,
            "rates_update_currency",
            serialize($request->getPost('rates_update_currency'))
        );

        $arRatesUpdateAgent['AGENT_INTERVAL'] = Option::get(ADMIN_MODULE_NAME, "rates_update_interval");

        if ($request->getPost('rates_update_time')) {
            $arRatesUpdateAgent['NEXT_EXEC'] = $request->getPost('rates_update_time');
        }

        CAgent::Update($arRatesUpdateAgent['ID'], $arRatesUpdateAgent);

        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_SAVED"),
            "TYPE" => "OK",
        ));
    } else {
        CAdminMessage::showMessage(Loc::getMessage("REFERENCES_INVALID_VALUE"));
    }
}

$rsCurrency = CCurrency::GetList($by = 'sort', $order = 'asc', LANG_ADMIN_LID);

$arCurrencies = Array();
while ($arCurrency = $rsCurrency->GetNext(false, false)) {
    $arCurrencies[] = $arCurrency;
}

$tabControl->begin();
?>

<form method="post"
      action="<?= sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID) ?>">
    <?php
    echo bitrix_sessid_post();
    $tabControl->beginNextTab();
    ?>
    <tr>
        <td width="40%">
            <label for="max_image_size"><?= Loc::getMessage("REFERENCES_RATES_UPDATE_TIME") ?>:</label>
        <td width="60%">
            <?= CalendarDate("rates_update_time",
                String::htmlEncode($arRatesUpdateAgent['NEXT_EXEC']),
                "", "47", "class=\"typeinput\""); ?>
        </td>
    </tr>
    <tr>
        <td width="40%">
            <label for="max_image_size"><?= Loc::getMessage("REFERENCES_RATES_UPDATE_INTERVAL") ?>:</label>
        <td width="60%">
            <input type="text"
                   size="50"
                   maxlength="5"
                   name="rates_update_interval"
                   value="<?= String::htmlEncode(Option::get(ADMIN_MODULE_NAME, "rates_update_interval", 86400)); ?>"
            />
        </td>
    </tr>
    <tr>
        <td width="40%">
            <label for="max_image_size"><?= Loc::getMessage("REFERENCES_RATES_UPDATE_CURRENCY") ?>:</label>
        <td width="60%">
            <select type="text"
                    size="5"
                    multiple
                    name="rates_update_currency[]"
            />
            <?
            $arRatesUpdateCurrency = unserialize(Option::get(ADMIN_MODULE_NAME, "rates_update_currency", 86400));
            foreach ($arCurrencies AS $arCurrency) { ?>
                <option
                    value="<?= $arCurrency['CURRENCY'] ?>"<? if (in_array($arCurrency['CURRENCY'], $arRatesUpdateCurrency)) { ?> selected=""<? } ?>><?= $arCurrency['FULL_NAME'] ?></option>
            <? } ?>
            </select>
        </td>
    </tr>

    <?php
    $tabControl->buttons();
    ?>
    <input type="submit"
           name="save"
           value="<?= Loc::getMessage("MAIN_SAVE") ?>"
           title="<?= Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
    />
    <input type="submit"
           name="restore"
           title="<?= Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           onclick="return confirm('<?= AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<?= Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
    />
    <?php
    $tabControl->end();
    ?>
</form>
