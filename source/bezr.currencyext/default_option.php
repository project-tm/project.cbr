<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();


defined('CURRENCY_EXT_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'bezr.currencyext');

$bezr_currencyext_default_option = array(
    "rates_update_interval" => "86400",
    "rates_update_currency" => null,
);
