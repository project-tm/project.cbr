<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['BCE_MODULE_NAME'] = 'Расширенные валюты';
$MESS['BCE_MODULE_DESCRIPTION'] = 'Данный модуль добавляет следующие возможности:
- Автоматическое обновление курсов валют.
';
$MESS['BCE_NOT_INSTALLED_DEPENDENCE'] = 'Не установлены зависимости модуля.
Модуль зависит от модуля Валюты.
';
