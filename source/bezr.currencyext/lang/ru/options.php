<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['REFERENCES_RATES_UPDATE_TIME'] = "Время следующего выполнения";
$MESS['REFERENCES_RATES_UPDATE_INTERVAL'] = "Интервал обновления агентов";
$MESS['REFERENCES_RATES_UPDATE_CURRENCY'] = "Обновляемые валюты. По умолчанию обновляются все валюты кроме базовой";
$MESS['REFERENCES_OPTIONS_RESTORED'] = "Восстановлены настройки по умолчанию";
$MESS['REFERENCES_OPTIONS_SAVED'] = "Настройки сохранены";
$MESS['REFERENCES_INVALID_VALUE'] = "Введено неверное значение";
