<?php
namespace Bezr\CurrencyExt\Rates;

use Bitrix\Currency;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\HttpClient;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

class Update
{
    private static $instance;

    private function __construct()
    {
    }

    public static function run()
    {
        $obUpdate = self::getInstance();

        $arUpdateCurrency = unserialize(Option::get(CURRENCY_EXT_MODULE_NAME, 'rates_update_currency'));

        foreach ($arUpdateCurrency AS $currency) {
            $obUpdate->requestRates($currency);
        }

        return '\Bezr\CurrencyExt\Rates\Update::run();';
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function requestRates($currency, $date = 'now')
    {
        global $DB, $APPLICATION;

        $baseCurrency = Currency\CurrencyManager::getBaseCurrency();
        $date = date('d.m.Y', strtotime($date));
        if ($baseCurrency == '') {
            $result['STATUS'] = 'ERROR';
            $result['MESSAGE'] = Loc::getMessage('BX_CURRENCY_GET_RATE_ERR_BASE_CURRENCY_ABSENT');
        } elseif ($date == '' || !$DB->IsDate($date)) {
            $result['STATUS'] = 'ERROR';
            $result['MESSAGE'] = Loc::getMessage('BX_CURRENCY_GET_RATE_ERR_DATE_RATE');
        } elseif ($currency == '') {
            $result['STATUS'] = 'ERROR';
            $result['MESSAGE'] = Loc::getMessage('BX_CURRENCY_GET_RATE_ERR_CURRENCY');
        } else {
            switch ($baseCurrency) {
                case 'UAH':
                    $url = 'http://pfsoft.com.ua//service/currency/?date=' . $DB->FormatDate($date, CLang::GetDateFormat('SHORT', LANGUAGE_ID), 'DMY');
                    break;
                case 'BYR':
                    $url = 'http://www.nbrb.by//Services/XmlExRates.aspx?ondate=' . $DB->FormatDate($date, CLang::GetDateFormat('SHORT', LANGUAGE_ID), 'Y-M-D');
                    break;
                case 'RUB':
                case 'RUR':
                    $url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . $DB->FormatDate($date, \CLang::GetDateFormat('SHORT', LANGUAGE_ID), 'D.M.Y');
                    break;
            }
            $http = new HttpClient();
            $data = $http->get($url);

            $charset = 'windows-1251';
            $matches = array();
            if (preg_match("/<" . "\?XML[^>]{1,}encoding=[\"']([^>\"']{1,})[\"'][^>]{0,}\?" . ">/i", $data, $matches)) {
                $charset = trim($matches[1]);
            }
            $data = preg_replace("#<!DOCTYPE[^>]+?>#i", '', $data);
            $data = preg_replace("#<" . "\\?XML[^>]+?\\?" . ">#i", '', $data);
            $data = $APPLICATION->ConvertCharset($data, $charset, SITE_CHARSET);

            $objXML = new \CDataXML();
            $res = $objXML->LoadString($data);
            if ($res !== false)
                $data = $objXML->GetArray();
            else
                $data = false;

            switch ($baseCurrency) {
                case 'UAH':
                    if (is_array($data) && count($data["ValCurs"]["#"]["Valute"]) > 0) {
                        for ($j1 = 0, $intCount = count($data["ValCurs"]["#"]["Valute"]); $j1 < $intCount; $j1++) {
                            if ($data["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"] == $currency) {
                                $result['STATUS'] = 'OK';
                                $result['RATE_CNT'] = (int)$data["ValCurs"]["#"]["Valute"][$j1]["#"]["Nominal"][0]["#"];
                                $result['RATE'] = (float)str_replace(",", ".", $data["ValCurs"]["#"]["Valute"][$j1]["#"]["Value"][0]["#"]);
                                break;
                            }
                        }
                    }
                    break;
                case 'BYR':
                    if (is_array($data) && count($data["DailyExRates"]["#"]["Currency"]) > 0) {
                        for ($j1 = 0, $intCount = count($data["DailyExRates"]["#"]["Currency"]); $j1 < $intCount; $j1++) {
                            if ($data["DailyExRates"]["#"]["Currency"][$j1]["#"]["CharCode"][0]["#"] == $currency) {
                                $result['STATUS'] = 'OK';
                                $result['RATE_CNT'] = (int)$data["DailyExRates"]["#"]["Currency"][$j1]["#"]["Scale"][0]["#"];
                                $result['RATE'] = (float)str_replace(",", ".", $data["DailyExRates"]["#"]["Currency"][$j1]["#"]["Rate"][0]["#"]);
                                break;
                            }
                        }
                    }
                    break;
                case 'RUB':
                case 'RUR':
                    if (is_array($data) && count($data["ValCurs"]["#"]["Valute"]) > 0) {
                        for ($j1 = 0, $intCount = count($data["ValCurs"]["#"]["Valute"]); $j1 < $intCount; $j1++) {
                            if ($data["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"] == $currency) {
                                $result['STATUS'] = 'OK';
                                $result['RATE_CNT'] = (int)$data["ValCurs"]["#"]["Valute"][$j1]["#"]["Nominal"][0]["#"];
                                $result['RATE'] = (float)str_replace(",", ".", $data["ValCurs"]["#"]["Valute"][$j1]["#"]["Value"][0]["#"]);
                                break;
                            }
                        }
                    }
                    break;
            }
        }
        if ($result['STATUS'] != 'OK') {
            $result['STATUS'] = 'ERROR';
            $result['MESSAGE'] = Loc::getMessage('BX_CURRENCY_GET_RATE_ERR_RESULT_ABSENT');
        } else {
            $newRateFields = Array(
                'CURRENCY' => $currency,
                'DATE_RATE' => $date,
                'RATE' => $result['RATE'],
                'RATE_CNT' => $result['RATE_CNT'],
            );
            $arRate = \CCurrencyRates::GetList($by = 'rate', $order = 'order', Array(
                'CURRENCY' => $date,
                'DATE_RATE' => $date
            ))->GetNext();
            if ($arRate) {
                $resRate = \CCurrencyRates::Update($arRate['ID'], $newRateFields);
            } else {
                $resRate = \CCurrencyRates::Add($newRateFields);
            }
            if (!$resRate) {
                $result['STATUS'] = 'ERROR';
                $result['MESSAGE'] = Loc::getMessage('BX_CURRENCY_GET_RATE_ERR_SET_RESULT');
            }
        }
        return $result;
    }

    private function __clone()
    {
    }
}